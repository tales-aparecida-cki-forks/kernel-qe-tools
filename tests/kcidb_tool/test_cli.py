"""Test cases for bkr2kcidb cli module."""

from importlib.resources import files
import io
import json
import os
from pathlib import Path
import tempfile
import unittest
from unittest import mock

from cki_lib import misc
from cki_lib.kcidb import ValidationError

from kernel_qe_tools.kcidb_tool import cli
from kernel_qe_tools.kcidb_tool.cmd_push2umb import UMB_TOPIC

ASSETS = files(__package__) / 'assets'


class TestMisc(unittest.TestCase):
    """Misc tests."""

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_invalid_subcommand(self, stderr_mock):
        """Check invalid subcommand."""
        expected_message = "error: argument command: invalid choice: 'any_subcommand_non_defined'"
        args = (
            "any_subcommand_non_defined "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())


class TestCreateSubCommandCommon(unittest.TestCase):
    """Tests for the common part for create."""

    def test_invalid_nvr(self):
        """Check create subcommand with a bad nvr value."""
        nvr = 'kernel-bad'
        expected_message = f'Invalid value for nvr: {nvr}'
        args = (
            "create "
            "--source beaker  "
            f"--input {ASSETS}/beaker.xml "
            "--output /tmp/kcidb_all.json "
            "--checkout checkout_id "
            "--origin bkr2kcidb "
            "--test_plan true "
            "--add-output-files job_url=https://jenkins/job/my_job/1 "
            "--contact username@redhat.com "
            f"--nvr {nvr}"
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertIn(expected_message, str(context.exception))

    def test_invalid_src_nvr(self):
        """Check create subcommand with a bad nvr value."""
        src_nvr = 'invalid_src_nvr'
        expected_message = f'Invalid value for source nvr: {src_nvr}'
        args = (
            "create "
            "--source beaker  "
            f"--input {ASSETS}/beaker.xml "
            "--output /tmp/kcidb_all.json "
            "--checkout checkout_id "
            "--origin bkr2kcidb "
            "--test_plan true "
            "--nvr kernel-5.14.0-300.el9 "
            "--add-output-files job_url=https://jenkins/job/my_job/1 "
            "--contact username@redhat.com "
            f"--src-nvr {src_nvr}"
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertIn(expected_message, str(context.exception))

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_without_contact(self, stderr_mock):
        """Check create subcommand without contact."""
        expected_message = 'error: the following arguments are required: --contact'
        args = (
            "create "
            "--source beaker  "
            f"--input {ASSETS}/beaker.xml "
            "--output /tmp/kcidb_all.json "
            "--checkout checkout_id "
            "--origin bkr2kcidb "
            "--test_plan true "
            "--add-output-files job_url=https://jenkins/job/my_job/1 "
            "--nvr kernel-5.14.0-300.el9 "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_without_checkout(self, stderr_mock):
        """Check create subcommand without checkout."""
        expected_message = 'error: the following arguments are required: -c/--checkout'
        args = (
            "create "
            "--source beaker  "
            f"--input {ASSETS}/beaker.xml "
            "--output /tmp/kcidb_all.json "
            "--origin bkr2kcidb "
            "--test_plan true "
            "--add-output-files job_url=https://jenkins/job/my_job/1 "
            "--nvr kernel-5.14.0-300.el9 "
            "--contact username@redhat.com "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_without_source(self, stderr_mock):
        """Check create subcommand without checkout."""
        expected_message = 'error: the following arguments are required: --source'
        args = (
            "create "
            f"--input {ASSETS}/beaker.xml "
            "--output /tmp/kcidb_all.json "
            "--checkout checkout_id "
            "--origin bkr2kcidb "
            "--test_plan true "
            "--add-output-files job_url=https://jenkins/job/my_job/1 "
            "--nvr kernel-5.14.0-300.el9 "
            "--contact username@redhat.com "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_without_input(self, stderr_mock):
        """Check create subcommand without input."""
        expected_message = 'error: the following arguments are required: -i/--input'
        args = (
            "create "
            "--source beaker  "
            "--output /tmp/kcidb_all.json "
            "--checkout checkout_id "
            "--origin bkr2kcidb "
            "--test_plan true "
            "--add-output-files job_url=https://jenkins/job/my_job/1 "
            "--nvr kernel-5.14.0-300.el9 "
            "--contact username@redhat.com "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_with_an_undefined_source(self, stderr_mock):
        """Check create subcommand with an undefined source."""
        expected_message = (
            "error: argument --source: invalid choice: 'undefined' "
            "(choose from 'beaker', 'testing-farm')"
        )
        args = (
            "create "
            "--source undefined "
            f"--input {ASSETS}/beaker.xml "
            "--output /tmp/kcidb_all.json "
            "--checkout checkout_id "
            "--origin bkr2kcidb "
            "--test_plan true "
            "--add-output-files job_url=https://jenkins/job/my_job/1 "
            "--nvr kernel-5.14.0-300.el9 "
            "--contact username@redhat.com "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())


class TestCreateSubCommandWithBeaker(unittest.TestCase):
    """Test the specific Beaker part of the create subcommand."""

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_when_everything_works(self, stdout_mock):
        """Test when everything works."""
        # Prepare test
        with tempfile.TemporaryDirectory() as tmp_dir:
            kcidb_file = Path(tmp_dir, 'kcidb.json')
            expected_message = f'File {kcidb_file.resolve()} wrote !!\n'
            args = (
                "create "
                "--source beaker  "
                f"--input {ASSETS}/beaker.xml "
                f"--output {kcidb_file.resolve()} "
                "--checkout checkout_id "
                "--builds-origin b_build "
                "--checkout-origin c_origin "
                "--tests-origin t_origin "
                "--test_plan true "
                "--add-output-files extra_file=https://someserver/url "
                "--nvr kernel-5.14.0-300.el9 "
                "--brew-task-id 47919042 "
                "--tests-provisioner-url https://jenkins/job/my_job/1 "
                "--contact username@redhat.com "
            ).split()
            # Run code
            cli.main(args)
            self.assertTrue(kcidb_file.is_file())

            # Check statistics (1 checkout, 1 build and 24 tests (19 tasks and 5 recipes)
            report = json.loads(kcidb_file.read_text(encoding='utf-8'))
            self.assertEqual(1, len(report['checkouts']))
            self.assertEqual(1, len(report['builds']))
            self.assertEqual(24, len(report['tests']))

            # Check STDOUT
            self.assertEqual(expected_message, stdout_mock.getvalue())


class TestCreateSubCommandWithTestingFarm(unittest.TestCase):
    """Test the specific Testing Farm part of the create subcommand."""

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_when_everything_works(self, stdout_mock):
        """Test when everything works."""
        # Prepare test
        with tempfile.TemporaryDirectory() as tmp_dir:
            kcidb_file = Path(tmp_dir, 'kcidb_testing_farm.json')
            expected_message = f'File {kcidb_file.resolve()} wrote !!\n'
            args = (
                "create "
                "--source testing-farm "
                f"--input {ASSETS}/testing_farm.xml "
                f"--output {kcidb_file.resolve()} "
                "--checkout checkout_id "
                "--builds-origin b_build "
                "--checkout-origin c_origin "
                "--tests-origin t_origin "
                "--test_plan true "
                "--add-output-files extra_file=https://someserver/url "
                "--nvr kernel-5.14.0-300.el9 "
                "--brew-task-id 47919042 "
                "--tests-provisioner-url https://jenkins/job/my_job/1 "
                "--contact username@redhat.com "
            ).split()
            # Run code
            cli.main(args)
            self.assertTrue(kcidb_file.is_file())

            # Check statistics (1 checkout, 1 build and 5 tests (2 system_provision))
            report = json.loads(kcidb_file.read_text(encoding='utf-8'))
            self.assertEqual(1, len(report['checkouts']))
            self.assertEqual(1, len(report['builds']))
            self.assertEqual(5, len(report['tests']))

            # Check STDOUT
            self.assertEqual(expected_message, stdout_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_when_the_parser_does_not_contain_any_tests(self, stderr_mock):
        """Test when the parser is not able to get any tests.."""
        # Prepare test
        with tempfile.TemporaryDirectory() as tmp_dir:
            kcidb_file = Path(tmp_dir, 'kcidb_testing_farm.json')
            warning_message = (
                "Can't get arch for testsuite /plans/security/security-internal, "
                "skipping it"
            )
            exit_message = 'The parser did not get any tests'
            args = (
                "create "
                "--source testing-farm "
                f"--input {ASSETS}/testing_farm_provision_error.xml "
                f"--output {kcidb_file.resolve()} "
                "--checkout checkout_id "
                "--builds-origin b_build "
                "--checkout-origin c_origin "
                "--tests-origin t_origin "
                "--test_plan true "
                "--add-output-files extra_file=https://someserver/url "
                "--nvr kernel-5.14.0-300.el9 "
                "--brew-task-id 47919042 "
                "--tests-provisioner-url https://jenkins/job/my_job/1 "
                "--contact username@redhat.com "
            ).split()
            # Run code
            with self.assertRaises(SystemExit) as context:
                cli.main(args)
            self.assertEqual(exit_message, context.exception.code)
            self.assertIn(warning_message, stderr_mock.getvalue())

            self.assertTrue(kcidb_file.is_file())


class TestMergeSubcommand(unittest.TestCase):
    """Tests for the subcommand merge."""

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_when_everything_works(self, stdout_mock):
        """Test when everything works."""
        # Prepare test
        with tempfile.TemporaryDirectory() as tmp_dir:
            kcidb_file = Path(tmp_dir, 'my_merged_kcidb.json')
            expected_message = f'File {kcidb_file.resolve()} wrote !!'
            args = (
                "merge "
                f"--result {ASSETS}/kcidb_input/good/kcidb_1.json "
                f"--result {ASSETS}/kcidb_input/good/kcidb_2.json "
                f"--result {ASSETS}/kcidb_input/good/kcidb_3.json "
                f"--output {kcidb_file.resolve()} "
            ).split()
            # Run code
            cli.main(args)
            self.assertTrue(kcidb_file.is_file())

            # Check statistics (1 checkout, 3 builds and 271 tests)
            report = json.loads(kcidb_file.read_text(encoding='utf-8'))
            self.assertEqual(2, len(report['checkouts']))
            self.assertEqual(3, len(report['builds']))
            self.assertEqual(6, len(report['tests']))

            # Check STDOUT
            self.assertIn(expected_message, stdout_mock.getvalue())

    def test_detect_duplicate_tests(self):
        """Test that duplicate test ids are found."""
        # Prepare test
        with tempfile.TemporaryDirectory() as tmp_dir:
            kcidb_file = Path(tmp_dir, 'my_merged_kcidb.json')
            args = (
                "merge "
                f"--result {ASSETS}/kcidb_input/dup_tests/kcidb_1.json "
                f"--result {ASSETS}/kcidb_input/dup_tests/kcidb_2.json "
                f"--output {kcidb_file.resolve()} "
            ).split()
            # Run code
            with self.assertRaises(ValueError) as context:
                cli.main(args)
            self.assertTrue(
                'already present, refusing to merge' in str(context.exception))


class TestPush2DWSubcommand(unittest.TestCase):
    """Tests for the subcommand push2dw."""

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_with_a_non_existing_file(self, stderr_mock):
        """Check push2dw when the file does not exist."""
        file_name = 'this_file_should_not_exist.json'
        expected_message = f"{file_name} is not a file or does not exist"
        args = (
            "push2dw "
            f"--input {file_name} "
            "--token any_token"
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('1', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('kernel_qe_tools.kcidb_tool.cmd_push2dw.validate_extended_kcidb_schema')
    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_an_error_in_the_kcidb_validation(self,
                                              stderr_mock,
                                              mock_validate_extended_kcidb_schema):
        """Check push2dw when the KCIDB validation fails."""
        file_name = f'{ASSETS}/kcidb_input/good/kcidb_1.json'
        cmd_error_msg = f'The file {file_name} is not a valid KCIDB file'
        exc_error_msg = 'Some error'
        args = (
            "push2dw "
            f"--input {file_name} "
            "--token any_token"
        ).split()
        mock_validate_extended_kcidb_schema.side_effect = ValidationError(exc_error_msg)
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('1', str(context.exception))
        self.assertIn(cmd_error_msg, stderr_mock.getvalue())
        self.assertIn(exc_error_msg, stderr_mock.getvalue())

    @mock.patch('kernel_qe_tools.kcidb_tool.cmd_push2dw.validate_extended_kcidb_schema',
                mock.Mock())
    @mock.patch('kernel_qe_tools.kcidb_tool.cmd_push2dw.Datawarehouse')
    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_an_error_with_datawarehouse(self, stderr_mock, mock_dw_api):
        """Check push2dw when we have an error with dw."""
        file_name = f'{ASSETS}/kcidb_input/good/kcidb_1.json'
        dw_url = 'http:/dw.cki.org'
        cmd_error_msg = f'Unable to send KCIDB data to {dw_url}'
        exc_error_msg = 'Some error'
        args = (
            "push2dw "
            f"--input {file_name} "
            f"--url {dw_url} "
            "--token any_token"
        ).split()
        mock_dw_api.side_effect = Exception(exc_error_msg)
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('1', str(context.exception))
        self.assertIn(cmd_error_msg, stderr_mock.getvalue())
        self.assertIn(exc_error_msg, stderr_mock.getvalue())

    @mock.patch('kernel_qe_tools.kcidb_tool.cmd_push2dw.validate_extended_kcidb_schema',
                mock.Mock())
    @mock.patch('kernel_qe_tools.kcidb_tool.cmd_push2dw.Datawarehouse', mock.Mock())
    @mock.patch('sys.stdout', new_callable=io.StringIO)
    # also testing getting the token from the env
    @mock.patch.dict(os.environ, {"DW_TOKEN": "any_token"})
    def test_when_it_works(self, stdout_mock):
        """Check push2dw when everything works."""
        file_name = f'{ASSETS}/kcidb_input/good/kcidb_1.json'
        dw_url = 'http:/dw.cki.org'

        validation_msg = f'Validating KCIDB format for {file_name} file'
        sending_msg = f'Sending KCIDB data to {dw_url}'
        done_msg = 'Done!'
        args = (
            "push2dw "
            f"--input {file_name} "
            f"--url {dw_url} "
        ).split()
        cli.main(args)
        self.assertIn(validation_msg, stdout_mock.getvalue())
        self.assertIn(sending_msg, stdout_mock.getvalue())
        self.assertIn(done_msg, stdout_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_missing_token(self, stderr_mock):
        """Check fail with missing token."""
        expected_message = "push2dw: error: the following arguments are required: --token"
        args = (
            "push2dw "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())


class TestPush2UMBSubcommand(unittest.TestCase):
    """Tests for the subcommand push2umb."""

    @mock.patch('kernel_qe_tools.kcidb_tool.cmd_push2umb.stomp.StompClient')
    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_when_it_works(self, stdout_mock, stomp_mock):
        """Check basic."""
        expected = 'https://datawarehouse.cki-project.org/kcidb/checkouts/checkout_id'
        kcidb_content = b'{"checkouts": [{"id": "checkout_id"}]}'
        with (
            misc.tempfile_from_string(kcidb_content) as kcidb_file,
            misc.tempfile_from_string(b'') as certfile
        ):
            args = (
                'push2umb '
                f'--input {kcidb_file} '
                f'--certificate {certfile} '
            ).split()
            cli.main(args)
        stomp_mock.return_value.send_message.assert_called_once_with(
            {'checkouts': [{'id': 'checkout_id'}]},
            UMB_TOPIC
        )
        self.assertIn(expected, stdout_mock.getvalue())

    @mock.patch('kernel_qe_tools.kcidb_tool.cmd_push2umb.stomp.StompClient')
    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_when_stomp_return_an_exception(self, stderr_mock, stomp_mock):
        """Check a stomp exeception."""
        generic_error = 'Unable to send KCIDB data through UMB'
        exception_error = 'Some message'
        stomp_mock.side_effect = Exception(exception_error)
        kcidb_content = b'{"checkouts": [{"id": "checkout_id"}]}'
        with (
            misc.tempfile_from_string(kcidb_content) as kcidb_file,
            misc.tempfile_from_string(b'') as certfile
        ):
            args = (
                'push2umb '
                f'--input {kcidb_file} '
                f'--certificate {certfile} '
            ).split()
            with self.assertRaises(SystemExit) as cm, self.assertRaises(Exception):
                cli.main(args)
        self.assertIn(generic_error, stderr_mock.getvalue())
        self.assertIn(exception_error, stderr_mock.getvalue())
        self.assertEqual(1, cm.exception.code)

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_when_the_certificate_does_not_exist(self, stderr_mock):
        """Check when certificate does not exist."""
        expected = 'cert.pem is not a file or does not exist'
        with misc.tempfile_from_string(b'') as kcidb_file:
            args = (
                'push2umb '
                f'--input {kcidb_file} '
                '--certificate cert.pem '
            ).split()
            with self.assertRaises(SystemExit) as cm:
                cli.main(args)
        self.assertIn(expected, stderr_mock.getvalue())
        self.assertEqual(1, cm.exception.code)

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_when_the_input_file_does_not_exist(self, stderr_mock):
        """Check when the input file does not exist."""
        expected = 'kcidb.json is not a file or does not exist'
        with misc.tempfile_from_string(b'') as certfile:
            args = (
                'push2umb '
                '--input /not/a/file/kcidb.json '
                f'--certificate {certfile} '
            ).split()
            with self.assertRaises(SystemExit) as cm:
                cli.main(args)
        self.assertIn(expected, stderr_mock.getvalue())
        self.assertEqual(1, cm.exception.code)

"""Test Clean Dict."""

from argparse import ArgumentTypeError
from importlib.resources import files
import unittest
import xml.etree.ElementTree as ET

from kernel_qe_tools.kcidb_tool import utils

ASSETS = files(__package__) / 'assets'


class TestUtils(unittest.TestCase):
    """Test Parser."""

    def test_clean_dict(self):
        """Check clean dict function."""
        some_empty_values = {'a': {}, 'b': [], 'c': None, 'd': '', 'e': 'some_value'}
        expected_some_empty_values = {'e': 'some_value'}
        all_keys_with_values = {
            'a': {'aa': 'bb'},
            'b': [1, 2],
            'c': 'foo',
            'd': 'some',
            'e': 'some'
        }
        cases = (
            ('Empty dict', {}, {}),
            ('Some empty values', some_empty_values, expected_some_empty_values),
            ('All non empty values', all_keys_with_values, all_keys_with_values),
            ('Keep boolean values', {'bool': False}, {'bool': False})
        )
        for description, original_dict, expected in cases:
            with self.subTest(description):
                self.assertCountEqual(expected, utils.clean_dict(original_dict))

    def test_slugify_a_string(self):
        """Check slugify."""
        cases = (
            ('Only alpha numeric charss', 'heLL0', 'heLL0'),
            ('String starting with one invalid chars', '=hell0', 'hell0'),
            ('String starting with several invalid chars', ' =hell0', 'hell0'),
            ('String ending with invalid chars', 'hell0= ', 'hell0'),
            ('String with several invalid chars in the middle', 'he= =ll0', 'he_ll0'),
            ('String with one invalid chars', 'he ll0', 'he_ll0'),
            ('String with two consecutive underscores after replacement', 'he=_ll0', 'he_ll0'),
            ('String with two consecutive underscore', 'he__ll0', 'he_ll0'),
            ('String starting with underscore', '_hell0', 'hell0'),
            ('String ending with  underscore', 'hell0_', 'hell0'),
            ('String starting with dash/es', '--hell0', 'hell0'),
            ('String ending with dash/es', 'hel--l0--', 'hel--l0'),
            ('String starting with dash and underscore', '-_hell0', 'hell0'),
            ('String ending with dash and underscore', 'hell0-_', 'hell0'),
            ('String with dashes and underscores at the beginning and at the end',
             '-_--___-hell0--__--', 'hell0'),
        )
        for description, string, expected in cases:
            with self.subTest(description):
                self.assertEqual(expected, utils.slugify(string))

    def test_get_int(self):
        """Check get_int function."""
        text_to_int = '123'
        self.assertEqual(123, utils.get_int(text_to_int))

        # Invalid cases
        text_to_int = 'abc'
        self.assertIsNone(utils.get_int(text_to_int))
        text_to_int = None
        self.assertIsNone(utils.get_int(text_to_int))

    def test_get_nvr(self):
        """Check get_nvr."""
        cases = (
            ('Normal kernel', 'kernel-5.14.0-1.el9', 'kernel', '5.14.0', '1.el9'),
            ('Kernel with debug', 'kernel-debug-5.14.0-1.el9', 'kernel-debug', '5.14.0', '1.el9'),
            ('Kernel RT', 'kernel-rt-4.18.0-372.9.1.rt7.166.el8', 'kernel-rt',
             '4.18.0', '372.9.1.rt7.166.el8'),
            ('Kernel RT with debug', 'kernel-rt-debug-4.18.0-372.9.1.rt7.166.el8',
             'kernel-rt-debug', '4.18.0', '372.9.1.rt7.166.el8'),
            ('Kernel Automotive', 'kernel-automotive-5.14.0-364.325.el9iv',
             'kernel-automotive', '5.14.0', '364.325.el9iv'),
            ('Kernel Automotive with debug', 'kernel-automotive-debug-5.14.0-364.325.el9iv',
             'kernel-automotive-debug', '5.14.0', '364.325.el9iv'),
            ('Kernel 64K', 'kernel-64k-5.14.0-1.el9', 'kernel-64k', '5.14.0', '1.el9'),
            ('Kernel 64K with debug', 'kernel-64k-debug-5.14.0-1.el9', 'kernel-64k-debug',
             '5.14.0', '1.el9'),
            ('Invalid kernel version', 'kernel-foo5.14.0.164.el9', None, None, None),
            ('Userspace package', 'vim-8.2.2637-19.el9', 'vim', '8.2.2637', '19.el9'),
            ('Userspace with dashes in name', 'rt-setup-2.1-4.el8', 'rt-setup', '2.1', '4.el8'),
            ('A package without x.y.z version', 'vim-8.2', None, None, None),
            ('Unified RT Kernel', 'kernel-rt-5.14.0-399.el9', 'kernel-rt', '5.14.0',
             '399.el9'),
            ('Unified RT Kernel with debug', 'kernel-rt-debug-5.14.0-399.el9',
             'kernel-rt-debug', '5.14.0', '399.el9'),
            ('Kpatch', 'kpatch-patch-4_18_0-305_103_1-1-3.el8_4', 'kpatch-patch-4_18_0-305_103_1',
             '1', '3.el8_4'),
        )

        for description, nvr, exp_name, exp_version, exp_release in cases:
            with self.subTest(description):
                name, version, release = utils.get_nvr(nvr)
                self.assertEqual(name, exp_name)
                self.assertEqual(version, exp_version)
                self.assertEqual(release, exp_release)

    def test_get_nvr_info(self):
        """Old function to keep compatiblity with seqe."""
        cases = (
            ('Normal kernel', 'kernel-5.14.0-1.el9', 'kernel', '5.14.0-1.el9'),
            ('Kernel with debug', 'kernel-debug-5.14.0-1.el9', 'kernel', '5.14.0-1.el9'),
            ('Invalid kernel version', 'kernel-foo5.14.0.164.el9', None, None),
        )
        for description, nvr, exp_name, exp_version_release in cases:
            with self.subTest(description):
                name, version_release = utils.get_nvr_info(nvr)
                self.assertEqual(name, exp_name)
                self.assertEqual(version_release, exp_version_release)

    def test_get_provenance_info(self):
        """Get provenance info."""
        cases = (
            {
                'description': 'Given an url and service_name',
                'function': 'executor',
                'url': 'https://some_server/url',
                'service_name': 'some_service',
                'misc': None,
                'expected': {
                    'function': 'executor',
                    'url': 'https://some_server/url',
                    'service_name': 'some_service'
                },
            },
            {
                'description': 'Given an url, service_name and misc',
                'function': 'executor',
                'url': 'https://some_server/url',
                'service_name': 'some_service',
                'misc': {'foo': 'bar'},
                'expected': {
                    'function': 'executor',
                    'url': 'https://some_server/url',
                    'service_name': 'some_service',
                    'misc': {'foo': 'bar'}
                },
            },
            {
                'description': 'Given an url without service_name',
                'function': 'executor',
                'url': 'https://some_server/url',
                'service_name': None,
                'misc': None,
                'expected': {
                    'function': 'executor',
                    'url': 'https://some_server/url',
                    'service_name': 'Unknown'
                },
            },
            {
                'description': 'Without an url',
                'function': 'executor',
                'url': None,
                'service_name': 'some_service',
                'misc': None,
                'expected': {},
            },
        )

        for case in cases:
            with self.subTest(case['description']):
                self.assertDictEqual(utils.get_provenance_info(case['function'],
                                                               case['url'],
                                                               case['service_name'],
                                                               case['misc']),
                                     case['expected'])

    def test_valid_emails(self):
        """Test valid emails."""
        cases = (
            {
                'description': 'Full Name and address',
                'input': 'Full Name <username@redhat.com>',
                'output': 'Full Name <username@redhat.com>'
            },
            {
                'description': 'Only address',
                'input': 'username@redhat.com',
                'output': 'username@redhat.com',
            },
            {
                'description': 'Only address with dot in the lef part',
                'input': 'user.name@redhat.com',
                'output': 'user.name@redhat.com',
            },
            {
                'description': 'Only address with underscore in the lef part',
                'input': 'user_name@redhat.com',
                'output': 'user_name@redhat.com',
            },
            {
                'description': 'Only address with hyphen in the lef part',
                'input': 'user-name@redhat.com',
                'output': 'user-name@redhat.com',
            },
            {
                'description': 'Multiples characters',
                'input': 'us.er-na_me@redhat.com',
                'output': 'us.er-na_me@redhat.com',
            }
        )
        for case in cases:
            with self.subTest(case['description']):
                self.assertEqual(utils.email_type(case['input']),
                                 case['output'])

    def test_invalid_emails(self):
        """Test invalid emails"""
        cases = (
            {
                'description': 'Full Name without <',
                'input': 'Full Name username@redhat.com>'
            },
            {
                'description': 'Full Name without <',
                'input': 'Full Name username@redhat.com>'
            },
            {
                'description': 'Full Name without < and >',
                'input': 'Full Name username@redhat.com'
            },
            {
                'description': 'Email without @',
                'input': 'usernameredhat.com'
            },
            {
                'description': 'Email without domain',
                'input': 'username'
            },
            {
                'description': 'Email with bad domain',
                'input': 'username@redhat'
            }
        )
        for case in cases:
            with self.subTest(case['description']):
                self.assertRaises(ArgumentTypeError,
                                  utils.email_type,
                                  case['input'])

    def test_get_output_files(self):
        """Ensure get_output_files works."""
        xml_content = """
        <logs>
          <log name="file_1.txt" href="http://srv/file_1.txt"/>
          <log name="file_2.txt" href="http://srv/file_2.txt"/>
        </logs>
        """
        expected_all_files = [
            {'name': 'file_1.txt', 'url': 'http://srv/file_1.txt'},
            {'name': 'file_2.txt', 'url': 'http://srv/file_2.txt'}
        ]
        expected_selected_files = [
            {'name': 'file_1.txt', 'url': 'http://srv/file_1.txt'},
        ]

        all_files = None
        selected_files = ['file_1.txt']

        logs = ET.fromstring(xml_content)

        cases = (
            ('All files', all_files, expected_all_files),
            ('Selected files', selected_files, expected_selected_files),
        )

        for description, file_selection, expected in cases:
            with self.subTest(description):
                self.assertEqual(expected, utils.get_output_files(logs, file_selection))

    def test_get_service_name_by_url(self):
        """Ensure get_service_name_by_url works."""
        cases = (
            ('Jenkins', 'https://some.jenkins.service.loc/jobs/gitlab_one', 'jenkins'),
            ('Gitlab', 'https://gitlab.com/project-one/jobs/1234', 'gitlab'),
            ('Other', 'https://other.ci.service', 'Unknown'),
        )
        for description, url, expected in cases:
            with self.subTest(description):
                self.assertEqual(expected, utils.get_service_name_by_url(url))

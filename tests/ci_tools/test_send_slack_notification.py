"""Check test slack notification."""
import io
import os
import unittest
from unittest import mock

from cki_lib import misc

from kernel_qe_tools.ci_tools import send_slack_notification

OS_DICT = {'SLACK_WEBHOOK_URL': 'https://hooks.slack.com/services/XXXX'}
MSG = 'Message'


class TestCLI(unittest.TestCase):
    """Test case for send slack notification."""

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_when_the_slack_variable_is_not_defined(self, stderr_mock):
        """Test without slack variable."""
        expected_message = 'Please set $SLACK_WEBHOOK_URL'
        args = (
            "--message-file some_file"
        ).split()
        self.assertEqual(1, send_slack_notification.main(args))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch.dict(os.environ, OS_DICT)
    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_when_the_file_does_not_exist(self, stderr_mock):
        """Test when the file does not exist."""
        file_name = 'this_file_nevershould_not_exist'
        expected_message = f'{file_name} is not a file or does not exist'
        args = (
            f"--message-file {file_name}"
        ).split()
        self.assertEqual(1, send_slack_notification.main(args))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch.dict(os.environ, {'SLACK_WEBHOOK_URL': 'invalid_url'})
    @mock.patch('kernel_qe_tools.ci_tools.send_slack_notification.WebhookClient')
    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_with_an_exception_in_the_library(self, stderr_mock, webhook_mock):
        """Test when the webhook library raises an exception."""
        expected_message = 'Error sending the message'
        exception_error = 'Is invalid_url a valid URL?'
        webhook_mock.side_effect = ValueError(exception_error)
        with (
            misc.tempfile_from_string(bytes(MSG, encoding='utf-8')) as message_file
        ):
            args = (
                f"--message-file {message_file}"
            ).split()
            self.assertEqual(1, send_slack_notification.main(args))
        self.assertIn(exception_error, stderr_mock.getvalue())
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch.dict(os.environ, OS_DICT)
    @mock.patch('kernel_qe_tools.ci_tools.send_slack_notification.WebhookClient')
    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_everything_works(self, stdout_mock, webhook_mock):
        """Test when everything works."""
        expected_message = 'Message sent successfully'
        webhook_mock.return_value.send.return_value = mock.MagicMock(status_code=200, body='ok')
        with (
            misc.tempfile_from_string(bytes(MSG, encoding='utf-8')) as message_file
        ):
            args = (
                f"--message-file {message_file}"
            ).split()
            send_slack_notification.main(args)
            self.assertEqual(0, send_slack_notification.main(args))
        self.assertTrue(webhook_mock.called)
        webhook_mock.return_value.send.assert_called_with(
            text=MSG,
            blocks=[{
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': MSG
                    }
                }]
        )
        self.assertIn(expected_message, stdout_mock.getvalue())

    @mock.patch.dict(os.environ, OS_DICT)
    @mock.patch('kernel_qe_tools.ci_tools.send_slack_notification.WebhookClient')
    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_with_a_non_valid_response(self, stderr_mock, webhook_mock):
        """Test when a non valid response."""
        status_code = 200
        body = 'Some Error'
        webhook_mock.return_value.send.return_value = mock.MagicMock(
            status_code=status_code,
            body=body)
        with (
            misc.tempfile_from_string(bytes(MSG, encoding='utf-8')) as message_file
        ):
            args = (
                f"--message-file {message_file}"
            ).split()
            send_slack_notification.main(args)
            self.assertEqual(1, send_slack_notification.main(args))
        self.assertTrue(webhook_mock.called)
        webhook_mock.return_value.send.assert_called_with(
            text=MSG,
            blocks=[{
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': MSG
                    }
                }]
        )
        self.assertIn(f'Body: \n{body}', stderr_mock.getvalue())
        self.assertIn(f'Status code: {status_code}', stderr_mock.getvalue())
        self.assertIn(send_slack_notification.ERROR_MSG, stderr_mock.getvalue())

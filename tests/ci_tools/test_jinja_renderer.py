"""Test for json_renderer."""
from importlib.resources import files
import io
import os
import pathlib
import unittest
from unittest import mock

from kernel_qe_tools.ci_tools import jinja_renderer
from tests.utils import run_in_a_sanbox_directory

ASSETS = files(__package__) / 'assets/jinja_renderer'


class TestClI(unittest.TestCase):
    """Test CLI."""

    expected_jinja_output = 'json value\nyaml value\nenv_var\n'
    maxDiff = None
    os.environ['MY_VAR'] = 'env_var'

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_cli_writing_to_console(self, mock_stdout):
        """Test CLI writing to a console."""
        args = (
            f'--input {ASSETS / "file.json"} '
            f'--input {ASSETS / "file.yaml"} '
            f'--template {ASSETS / "template.j2"} '
        ).split()
        self.assertEqual(0, jinja_renderer.main(args))
        self.assertEqual(self.expected_jinja_output, mock_stdout.getvalue())

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_cli_writing_to_a_file(self, mock_stdout):
        """Test CLI ending without CI Jobs."""
        expected_warning = 'The file file.txt will not be used.'
        args = (
            f'--input {ASSETS / "file.json"} '
            f'--input {ASSETS / "file.yaml"} '
            f'--input file.txt '
            f'--template {ASSETS / "template.j2"} '
            '--output output.txt '
        ).split()
        with run_in_a_sanbox_directory():
            self.assertEqual(0, jinja_renderer.main(args))
            content = pathlib.Path('output.txt').read_text(encoding='utf-8')

        self.assertEqual(self.expected_jinja_output, content)
        self.assertIn(expected_warning, mock_stdout.getvalue())

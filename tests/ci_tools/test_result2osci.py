import unittest
from unittest import mock

from kernel_qe_tools.ci_tools import result2osci


class TestResult2osci(unittest.TestCase):
    """Test pusblish result to OSCI."""

    @mock.patch('kernel_qe_tools.ci_tools.result2osci.get_brew_brew_build')
    @mock.patch('kernel_qe_tools.ci_tools.result2osci.stomp.StompClient')
    def test_result2osci_main_valid(self, mock_stomp, mock_brewbuild):
        """Smoke test with a valid parameters."""
        mock_brewbuild.return_values = {}
        mock_stomp.return_values = True
        result = result2osci.main(["--source-nvr", "test-1234-45.el9",
                                   "--log-url", "https://test.com/log",
                                   "--run-url", "https://test.com/run",
                                   "--pipeline-id", "123456",
                                   "--test-status", "complete",
                                   "--test-result", "passed",
                                   "--certificate", "/tmp/certificate"])
        self.assertEqual(result, 0)

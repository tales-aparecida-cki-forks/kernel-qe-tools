"""Beaker parser to kcidb."""

import re

from cki_lib.kcidb import sanitize_kcidb_status
from cki_lib.logger import get_logger

from . import utils as bkr_utils
from .. import utils
from ..parser import KCIDBToolParser

LOGGER = get_logger(__name__)


class Bkr2KCIDBParser(KCIDBToolParser):
    """Parser."""

    DEFAULT_TEST_LOG_FILENAME = 'taskout.log'

    def __init__(self, bkr_content, args):
        """Initialize object with the given data."""
        super().__init__(bkr_content, args)
        self.job_whiteboard = ''
        self.recipe_info = {}

    def add_checkout(self):
        """Add checkout."""
        # pylint: disable=duplicate-code
        self.report['checkout'] = utils.clean_dict({
            'id': self.checkout_id,
            'origin': self.checkout_origin,
            'tree_name': self.get_tree_name(),
            'valid': True,
            'misc': self.add_checkout_misc_info(),
            'start_time': self.get_oldest_start_time()
        })

    def add_build(self):
        """Add build if it is not present."""
        # pylint: disable=duplicate-code
        if not self.exist_build_by_arch():
            self.report['builds'].append({
                'id': self._get_build_id(),
                'origin': self.builds_origin,
                'checkout_id': self.checkout_id,
                'architecture': self.arch,
                'valid': True,
                'misc': self.add_build_misc_info()
            })

    def get_results(self, test, results):
        """
        Generate results for a test.

        We can process directly all results except results added by Beaker on
        top the Restraint results.

        When a test is Aborted, Beaker always add a result following this criteria:
        * The path field is equal to '/'
        * The result field is equal to 'Warn'
        * The result contains a text with the Beaker Information

        If a status of the test is 'MISS', all subtest will use 'MISS' as well.

        If the test finds a local watch dog (/10_localwatchdog), the parser must process it, stop
        and returnst all results until the local watch dog (watchdog included).
        """
        # pylint: disable=no-self-use
        all_results = []
        for number, result in enumerate(results.iter('result'), 1):
            path = result.get('path')
            _result = result.get('result')

            if path == '/' and _result in ['Warn', 'Panic']:
                name = result.text
            else:
                name = path

            if test['status'] == 'MISS':
                subtest_status = test['status']
                LOGGER.info('Forcing subtest(%s).status=MISS because test.status=MISS',
                            f'{test["id"]}.{number}')
            else:
                subtest_status = sanitize_kcidb_status(_result)

            all_results.append(utils.clean_dict({
                'id': f'{test["id"]}.{number}',
                'name': name or 'UNDEFINED',  # deprecated
                'comment': name or 'UNDEFINED',
                'status': subtest_status,
                'output_files': utils.get_output_files(result.find('logs')),
            }))

            # Stop processing if the parser finds '/10_localwatchdog'
            if '/10_localwatchdog' in path:
                break

        return all_results

    def add_test_misc_info(self, task, test):
        """Return misc for a test."""
        misc = {}

        results = task.find('results')
        test_source = task.find('fetch')

        if results is not None:
            misc['results'] = self.get_results(test, results)

        if test_source is not None:  # 'if test_source' does not work
            misc['fetch_url'] = test_source.get('url')

        beaker_url = bkr_utils.get_recipe_url(self.recipe_info['id'])
        provenance_misc = {
            'job_id': self.recipe_info['job_id'],
            'job_whiteboard': self.job_whiteboard,
            'recipe_id': self.recipe_info['id'],
            'recipe_set_id': self.recipe_info['recipe_set_id'],
            'recipe_whiteboard': self.recipe_info['whiteboard'],
            'task_id': task.get('id')
        }
        misc['provenance'] = [utils.get_provenance_info('executor',
                                                        beaker_url,
                                                        'beaker',
                                                        provenance_misc
                                                        )]
        if self.args.tests_provisioner_url:
            misc['provenance'].append(utils.get_provenance_info('provisioner',
                                                                self.args.tests_provisioner_url
                                                                ))

        misc['maintainers'] = bkr_utils.get_tasks_maintainers(task) or self.default_maintainers

        return utils.clean_dict(misc)

    def add_test(self, task, test_id):
        """Add test."""
        test = {
            'id': self._get_test_id(test_id),
            'origin': self.tests_origin,
            'build_id': self._get_build_id(),
            'comment': task.get('name'),
            'path': utils.slugify(task.get('name')),
        }
        if not self.args.test_plan:
            test['start_time'] = bkr_utils.get_utc_datetime(task.get('start_time'))
            test['duration'] = bkr_utils.get_duration(task.get('duration', '00:00:00'))
            test['output_files'] = utils.get_output_files(task.find('logs')) + \
                self.get_extra_output_files() + \
                bkr_utils.get_output_console_log(self.recipe_info['id'])
            if self.recipe_info['installation_failed'] and task.get('fake', '') != 'True':
                test['status'] = 'MISS'
            else:
                test['status'] = bkr_utils.sanitize_test_status(task)
            test['misc'] = self.add_test_misc_info(task, test)

            test['log_url'] = self.get_default_test_log(test['output_files'])

        test['environment'] = utils.clean_dict({'comment': self.recipe_info['system']})

        self.report['tests'].append(utils.clean_dict(test))

    def get_tree_name(self):
        """Get tree_name from the first distro field of any recipe."""
        regxep = re.compile(r'(rhel-\d+\.\d+)')
        for recipe in self.root.iter('recipe'):
            if 'distro' in recipe.attrib:
                tree_name = recipe.get('distro').lower()
                re_match = regxep.match(tree_name)
                if re_match:
                    return re_match.group(1)
        return None

    def get_oldest_start_time(self):
        """
        Get the oldest start time looking into recipes.

        It returns the oldest start time in all recipes, otherwise it returns None
        """
        dates = [recipe.get('start_time') for recipe in self.root.iter('recipe')
                 if 'start_time' in recipe.attrib]

        if dates:
            return bkr_utils.get_utc_datetime(min(dates))
        return None

    def process(self):
        """Iterate through all recipes to generate the report."""
        self._clean_report()
        self.add_checkout()
        self.job_whiteboard = 'UNDEFINED'
        test_id = 1
        if (whiteboard := self.root.find('whiteboard')) is not None:
            self.job_whiteboard = whiteboard.text
        for recipe in self.root.iter('recipe'):
            # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-qe-tools/-/issues/15
            # i386 is not supported by DW
            self.arch = recipe.get('arch').replace('i386', 'i686')
            self.recipe_info = {
                'job_id': recipe.get('job_id'),
                'id': recipe.get('id'),
                'recipe_set_id': recipe.get('recipe_set_id'),
                'system': recipe.get('system'),
                'whiteboard': recipe.get('whiteboard', 'UNDEFINED')
            }
            self.add_build()
            system_provision_task = bkr_utils.create_system_provision_task(recipe, test_id)
            self.recipe_info['installation_failed'] = system_provision_task.get('result') == 'FAIL'
            self.add_test(system_provision_task, f'{self.recipe_info["id"]}_{test_id}')
            test_id += 1
            for task in recipe.iter('task'):
                self.add_test(task, f'{self.recipe_info["id"]}_{test_id}')
                test_id += 1
        self.processed = True

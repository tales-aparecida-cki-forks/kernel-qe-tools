"""Dataclasses."""
from dataclasses import dataclass
import typing


@dataclass
class ExternalOutputFile:
    """External OutputFile."""

    name: str
    url: str


@dataclass
class NVR:
    """NVR representation."""

    name: str
    version: str
    release: str


@dataclass
class ParserArguments:
    """Arguments to pass to a bkr2kcidb parser.

    All external arguments should be here
    """

    # pylint: disable=too-many-instance-attributes
    brew_task_id: str
    builds_origin: str
    checkout: str
    checkout_origin: str
    contacts: typing.List[str]
    extra_output_files: typing.Optional[typing.List[ExternalOutputFile]]
    nvr: NVR
    src_nvr: NVR
    test_plan: bool
    tests_origin: str
    tests_provisioner_url: str

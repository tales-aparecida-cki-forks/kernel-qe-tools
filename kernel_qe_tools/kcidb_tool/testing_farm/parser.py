"""Testing Farm parser to kcidb."""

import datetime
import hashlib
import re
import sys

from cki_lib import misc as cki_lib_misc
from cki_lib.logger import get_logger

from . import utils as testing_farm_utils
from .. import utils
from ..parser import KCIDBToolParser

LOGGER = get_logger(__name__)


class TestingFarmParser(KCIDBToolParser):
    """Parser."""

    DEFAULT_TEST_LOG_FILENAME = 'testout.log'
    __test__ = False

    def __init__(self, testing_farm_content, args):
        """Initialize object with the given data."""
        super().__init__(testing_farm_content, args)
        self.test_suite_info = {}
        self.common_logs = ['console log', 'workdir']
        self.start_time = cki_lib_misc.now_tz_utc()

    def add_checkout(self):
        """Add checkout."""
        self.report['checkout'] = utils.clean_dict({
            'id': self.checkout_id,
            'origin': self.args.checkout_origin or self.args.origin,
            'tree_name': self.get_tree_name(),
            'valid': True,
            'misc': self.add_checkout_misc_info(),
            'start_time': self.start_time.isoformat()
        })

    def add_build(self):
        """Add build if it is not present."""
        if not self.exist_build_by_arch():
            self.report['builds'].append({
                'id': self._get_build_id(),
                'origin': self.args.builds_origin or self.args.origin,
                'checkout_id': self.checkout_id,
                'architecture': self.test_suite_info['arch'],
                'valid': True,
                'misc': self.add_build_misc_info()
            })

    def add_test_misc_info(self, test_case, test):
        """Return misc for a test."""
        misc = {}
        # Default value is 2 for real testcases, because 1 is used by the virtual one.
        first_id_for_non_virtual_results = 2
        misc["results"] = []
        # Only add virtual result to real test cases
        if test_case.get('fake', 'false') != 'True':
            misc["results"] += [testing_farm_utils.create_virtual_result(test)]
        else:
            first_id_for_non_virtual_results = 1
        results = test_case.find('checks')
        if results is not None:
            misc['results'] += testing_farm_utils.get_results(test,
                                                              results,
                                                              first_id_for_non_virtual_results)

        if self.args.tests_provisioner_url:
            misc['provenance'] = [utils.get_provenance_info('provisioner',
                                                            self.args.tests_provisioner_url
                                                            )]

        misc['maintainers'] = self.default_maintainers

        return utils.clean_dict(misc)

    def add_test(self, test_case, test_case_id):
        """Add test."""
        test = {
            'id': self._get_test_id(test_case_id),
            'origin': self.args.tests_origin or self.args.origin,
            'build_id': self._get_build_id(),
            'comment': test_case.get('name'),
            'path': utils.slugify(test_case.get('name')),
        }
        if not self.args.test_plan:
            test['start_time'] = self.test_suite_info['start_time'].isoformat()
            test['duration'] = int(test_case.get('time', '0'))
            test['output_files'] = utils.get_output_files(test_case.find('logs')) + \
                self.get_extra_output_files() + \
                self.test_suite_info['common_logs']
            test['status'] = testing_farm_utils.get_kcidb_test_status(test_case.get('result'))
            test['misc'] = self.add_test_misc_info(test_case, test)

            test['environment'] = utils.clean_dict({
                'comment': testing_farm_utils.get_hostname_from_testcase(test_case)
            })
            # update test suite start time
            self.test_suite_info['start_time'] += datetime.timedelta(seconds=test['duration'])

            test['log_url'] = self.get_default_test_log(test['output_files'])

        self.report['tests'].append(utils.clean_dict(test))

    def get_tree_name(self):
        """Get the treename from the first test suite."""
        regxep = re.compile(r'(rhel-\d+\.\d+)')
        prop = self.root.find(
            ".testsuite/testing-environment[@name='provisioned']/property[@name='compose']"
        )
        value = None
        try:
            compose = prop.get('value', 'UNDEFINED').lower()
            re_match = regxep.match(compose)
            if re_match:
                value = re_match[1]
        except AttributeError:
            pass
        return value

    def process(self):
        """Iterate through all testsuites to generate the report."""
        self._clean_report()
        self.add_checkout()
        start_time = self.start_time
        test_case_id = 1

        # We need something to make the test-id unique for the case of
        # merging multiple results together.  Since the logs are
        # stored in unique directories, making a hash of the "<log
        # href=" values gives us something unique enough.
        uniq = hashlib.sha256()
        for log in self.root.findall(".//log[@href]"):
            uniq.update(log.attrib['href'].encode('utf-8'))
        test_tag = uniq.hexdigest()[0:5]

        for test_suite in self.root.iter('testsuite'):
            # Testing Farm ISSUE https://issues.redhat.com/projects/TFT/issues/TFT-2641
            # When the sut can not provisioned testing does not provide any information
            # about arch and compose
            test_suite_name = test_suite.get('name', 'UNDEFINED')
            arch = testing_farm_utils.get_arch_from_testsuite(test_suite)
            if arch is None:
                print(f"Can't get arch for testsuite {test_suite_name}, skipping it",
                      file=sys.stderr)
                continue
            self.test_suite_info = {
                'arch': arch,
                'whiteboard': test_suite_name,
                'start_time': start_time,
                'common_logs': utils.get_output_files(test_suite.find('logs'), self.common_logs)
            }
            self.arch = self.test_suite_info['arch']
            self.add_build()
            # Add the system provision test suite
            system_provision = testing_farm_utils.create_system_provision_test_case(test_suite,
                                                                                    test_case_id)
            self.add_test(system_provision, f'{test_tag}_{test_case_id}')
            test_case_id += 1
            for test_case in test_suite.iter('testcase'):
                self.add_test(test_case, f'{test_tag}_{test_case_id}')
                test_case_id += 1
            start_time = self.test_suite_info['start_time']
        self.processed = True

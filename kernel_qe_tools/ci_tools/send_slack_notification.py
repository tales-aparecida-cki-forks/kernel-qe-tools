"""Send message to slack channel."""
import argparse
import os
import pathlib
import sys
import typing

from slack_sdk.webhook import WebhookClient

ERROR_MSG = 'Error sending the message'


def main(args: typing.Optional[typing.List[str]] = None) -> int:
    """Send the provided message to the slack channel based on webhok url."""
    parser = argparse.ArgumentParser(description='Send message to channel using webhook')
    parser.add_argument(
        '--message-file',
        type=pathlib.Path,
        required=True
    )
    parsed_args = parser.parse_args(args)

    # Check parameters
    try:
        url = os.environ["SLACK_WEBHOOK_URL"]
    except KeyError:
        print("Please set $SLACK_WEBHOOK_URL", file=sys.stderr)
        return 1

    if not parsed_args.message_file.is_file():
        print(f'{parsed_args.message_file} is not a file or does not exist', file=sys.stderr)
        return 1

    # Send the request
    message = parsed_args.message_file.read_text(encoding='utf-8')

    try:
        response = WebhookClient(url).send(
            text=message,
            blocks=[
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": message
                    }
                }
            ]
        )
    except ValueError:
        print(ERROR_MSG, file=sys.stderr)
        print(f'Is {url} a valid URL?', file=sys.stderr)
        return 1

    # Check request response
    if not (response.status_code == 200 and response.body == 'ok'):
        print(f'Body: \n{response.body}', file=sys.stderr)
        print(f'Status code: {response.status_code}', file=sys.stderr)
        print(ERROR_MSG, file=sys.stderr)
        return 1

    print('Message sent successfully')
    return 0


if __name__ == "__main__":  # pragma: no cover
    sys.exit(main(sys.argv[1:]))
